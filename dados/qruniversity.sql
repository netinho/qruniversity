SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `netinhoi_qruniversity` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `netinhoi_qruniversity` ;

-- -----------------------------------------------------
-- Table `netinhoi_qruniversity`.`Sala`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `netinhoi_qruniversity`.`Sala` (
  `id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificação',
  `nome` VARCHAR(150) NOT NULL COMMENT 'Nome',
  `sigla` VARCHAR(20) NOT NULL COMMENT 'Sigla',
  `hash` VARCHAR(60) NULL COMMENT 'Hash da Sigla',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Salas';


-- -----------------------------------------------------
-- Table `netinhoi_qruniversity`.`Professor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `netinhoi_qruniversity`.`Professor` (
  `id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificação',
  `nome` VARCHAR(150) NOT NULL COMMENT 'Nome',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Professores';


-- -----------------------------------------------------
-- Table `netinhoi_qruniversity`.`Disciplina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `netinhoi_qruniversity`.`Disciplina` (
  `id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificação',
  `nome` VARCHAR(45) NOT NULL COMMENT 'Nome',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Disciplinas';


-- -----------------------------------------------------
-- Table `netinhoi_qruniversity`.`Semestre`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `netinhoi_qruniversity`.`Semestre` (
  `id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificação',
  `nome` VARCHAR(45) NOT NULL COMMENT 'Nome',
  `sigla` CHAR(5) NOT NULL COMMENT 'Sigla',
  `inicio` DATE NOT NULL COMMENT 'Data de Início',
  `termino` DATE NOT NULL COMMENT 'Data de Término',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Semestres';


-- -----------------------------------------------------
-- Table `netinhoi_qruniversity`.`DiaSemana`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `netinhoi_qruniversity`.`DiaSemana` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificação',
  `nome` VARCHAR(25) NOT NULL COMMENT 'Nome',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Dias da Semana';


-- -----------------------------------------------------
-- Table `netinhoi_qruniversity`.`Aula`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `netinhoi_qruniversity`.`Aula` (
  `id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Identificação',
  `semestre` BIGINT NOT NULL COMMENT 'Semestre',
  `sala` BIGINT NOT NULL COMMENT 'Sala',
  `disciplina` BIGINT NOT NULL COMMENT 'Disciplina',
  `professor` BIGINT NOT NULL COMMENT 'Professor',
  `turma` VARCHAR(5) NOT NULL COMMENT 'Turma',
  `diaSemana` INT NOT NULL COMMENT 'Dia da Semana\n',
  `inicio` TIME NOT NULL COMMENT 'Hora de Início',
  `termino` TIME NOT NULL COMMENT 'Hora de Término',
  PRIMARY KEY (`id`),
  INDEX `fk_Aula_Semestre_idx` (`semestre` ASC),
  INDEX `fk_Aula_Sala1_idx` (`sala` ASC),
  INDEX `fk_Aula_Disciplina1_idx` (`disciplina` ASC),
  INDEX `fk_Aula_Professor1_idx` (`professor` ASC),
  INDEX `fk_Aula_DiaSemana1_idx` (`diaSemana` ASC),
  CONSTRAINT `fk_Aula_Semestre`
    FOREIGN KEY (`semestre`)
    REFERENCES `netinhoi_qruniversity`.`Semestre` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Aula_Sala`
    FOREIGN KEY (`sala`)
    REFERENCES `netinhoi_qruniversity`.`Sala` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Aula_Disciplina`
    FOREIGN KEY (`disciplina`)
    REFERENCES `netinhoi_qruniversity`.`Disciplina` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Aula_Professor`
    FOREIGN KEY (`professor`)
    REFERENCES `netinhoi_qruniversity`.`Professor` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Aula_DiaSemana1`
    FOREIGN KEY (`diaSemana`)
    REFERENCES `netinhoi_qruniversity`.`DiaSemana` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Aulas';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `netinhoi_qruniversity`.`DiaSemana`
-- -----------------------------------------------------
START TRANSACTION;
USE `netinhoi_qruniversity`;
INSERT INTO `netinhoi_qruniversity`.`DiaSemana` (`id`, `nome`) VALUES (1, 'Domingo');
INSERT INTO `netinhoi_qruniversity`.`DiaSemana` (`id`, `nome`) VALUES (2, 'Segunda');
INSERT INTO `netinhoi_qruniversity`.`DiaSemana` (`id`, `nome`) VALUES (3, 'Terça');
INSERT INTO `netinhoi_qruniversity`.`DiaSemana` (`id`, `nome`) VALUES (4, 'Quarta');
INSERT INTO `netinhoi_qruniversity`.`DiaSemana` (`id`, `nome`) VALUES (5, 'Quinta');
INSERT INTO `netinhoi_qruniversity`.`DiaSemana` (`id`, `nome`) VALUES (6, 'Sexta');
INSERT INTO `netinhoi_qruniversity`.`DiaSemana` (`id`, `nome`) VALUES (NULL, 'Sábado');

COMMIT;

USE `netinhoi_qruniversity`;

DELIMITER $$
USE `netinhoi_qruniversity`$$


CREATE TRIGGER `Sala_AINS` AFTER INSERT ON `Sala` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
  SET new.hash = MD5(new.sigla);
END$$

USE `netinhoi_qruniversity`$$


CREATE TRIGGER `Sala_AUPD` AFTER UPDATE ON `Sala` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
  SET new.hash = MD5(new.sigla);
END$$


DELIMITER ;
