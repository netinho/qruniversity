package br.upis.tap.qruniversity.entity.dto;

public class Aula {

	private String sala;
	private String turma;
	private String inicio;
	private String termino;
	private String professor;
	private String materia;

	public Aula(String sala, String turma, String inicio, String termino,
			String professor, String materia) {

		this.sala = sala;
		this.turma = turma;
		this.inicio = inicio;
		this.termino = termino;
		this.professor = professor;
		this.materia = materia;
	}

	public String getSala() {
		return sala;
	}

	public String getInicio() {
		return inicio;
	}

	public String getTermino() {
		return termino;
	}

	public String getProfessor() {
		return professor;
	}

	public String getMateria() {
		return materia;
	}

	public String getTurma() {
		return turma;
	}

}
