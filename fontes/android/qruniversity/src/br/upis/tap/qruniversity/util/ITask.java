package br.upis.tap.qruniversity.util;

import java.util.List;

import br.upis.tap.qruniversity.entity.dto.Aula;

public interface ITask {

	public void onCompleted(List<Aula> list);

}
