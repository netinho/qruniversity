package br.upis.tap.qruniversity.controller;

import java.util.List;

import br.upis.tap.qruniversity.entity.dto.Aula;

import android.app.Application;

public class SessionApplication extends Application {

	private static SessionApplication app;
	private List<Aula> aulas;

	private SessionApplication() {
	}

	public static SessionApplication getInstance() {
		if (app == null) {
			app = new SessionApplication();
		}

		return app;
	}

	public void setAulas(List<Aula> aulas) {
		this.aulas = aulas;
	}

	public List<Aula> getAulas() {
		return aulas;
	}

}
