package br.upis.tap.qruniversity.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.upis.tap.qruniversity.R;
import br.upis.tap.qruniversity.entity.dto.Aula;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AulaAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private List<Aula> lista;

	private enum Horario {
		AFTER, BEFORE, NOW
	}

	public AulaAdapter(Context ctx, List<Aula> lista) {
		this.lista = lista;
		inflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Aula aula = lista.get(position);

		Horario horario = isNow(aula.getInicio(), aula.getTermino());

		View view = inflater.inflate(R.layout.list_aulas, null);

		TextView materia = (TextView) view.findViewById(R.id.materia);
		materia.setText(aula.getMateria());

		TextView professor = (TextView) view.findViewById(R.id.professor);
		professor.setText(aula.getProfessor());

		TextView turma = (TextView) view.findViewById(R.id.turma);
		turma.setText("Turma: " + aula.getTurma());

		TextView status = (TextView) view.findViewById(R.id.status);

		int drawable = R.drawable.aula_proxima_shape;

		if (horario == Horario.NOW) {
			drawable = R.drawable.aula_atual_shape;
		}

		Drawable draw = inflater.getContext().getResources()
				.getDrawable(drawable);
		status.setBackgroundDrawable(draw);

		if (horario == Horario.BEFORE) {
			view.setVisibility(View.GONE);
		}

		return view;
	}

	@SuppressLint("SimpleDateFormat")
	private Horario isNow(String inicio, String termino) {

		Horario result = Horario.BEFORE;

		try {
			if (inicio != null && inicio.length() > 0 && termino != null
					&& termino.length() > 0) {

				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

				Date dateInicio = sdf.parse(inicio);
				Date dateTermino = sdf.parse(termino);
				Date dateAtual = new Date();

				if (dateAtual.getTime() > dateTermino.getTime()) {
					result = Horario.BEFORE;
				} else if (dateAtual.getTime() > dateInicio.getTime()
						&& dateAtual.getTime() < dateTermino.getTime()) {
					result = Horario.NOW;
				} else {
					result = Horario.AFTER;
				}

			}

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return result;
	}
}
