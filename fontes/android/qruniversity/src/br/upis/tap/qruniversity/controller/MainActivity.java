package br.upis.tap.qruniversity.controller;

import br.upis.tap.qruniversity.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	private Button btnCaptureScreen;

	// private Button btnAboutScreen;
	// private Button btnConfigureScreen;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		btnCaptureScreen = (Button) findViewById(R.id.btn_capture_screen);
		btnCaptureScreen.setOnClickListener(captureListener);

		// btnAboutScreen = (Button) findViewById(R.id.btn_about_screen);
		// btnAboutScreen.setVisibility(Button.INVISIBLE);

		// btnConfigureScreen = (Button)
		// findViewById(R.id.btn_configure_screen);
		// btnConfigureScreen.setVisibility(Button.INVISIBLE);
	}

	private View.OnClickListener captureListener = new View.OnClickListener() {

		public void onClick(View v) {
			Intent it = new Intent(MainActivity.this, CaptureActivity.class);
			startActivity(it);
		}

	};

}