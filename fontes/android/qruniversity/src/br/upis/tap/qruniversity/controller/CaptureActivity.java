package br.upis.tap.qruniversity.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import br.upis.tap.qruniversity.R;
import br.upis.tap.qruniversity.camera.CameraSurfaceView;
import br.upis.tap.qruniversity.entity.dto.Aula;
import br.upis.tap.qruniversity.service.ResultQRLoader;
import br.upis.tap.qruniversity.util.ITask;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;

public class CaptureActivity extends Activity implements ITask {

	private Button btnCaptureClose;
	private Button btnCaptureGotcha;

	private CameraSurfaceView surface;
	private Handler autoFocusHandler;

	private boolean scanned = false;
	private String baseUrl = "http://qruniversity.francisco.pro/gerarJson.php";
	private String dataUrl;

	private ConnectivityManager cm;
	private SessionApplication session;

	static {
		System.loadLibrary("iconv");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_capture);

		btnCaptureClose = (Button) findViewById(R.id.btn_capture_close);
		btnCaptureGotcha = (Button) findViewById(R.id.btn_capture_gotcha);

		btnCaptureClose.setOnClickListener(close);
		btnCaptureGotcha.setOnClickListener(startScanner);

		autoFocusHandler = new Handler();

		FrameLayout frame = (FrameLayout) findViewById(R.id.frameLayout);
		surface = new CameraSurfaceView(this);

		frame.addView(surface);

		cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		session = SessionApplication.getInstance();

	}
	
	@Override
	protected void onResume() {
	    surface.start();
		super.onResume();
	}
	
	@Override
	protected void onPause() {
	    surface.stop();
		super.onPause();
	}

	private OnClickListener startScanner = new OnClickListener() {

		@Override
		public void onClick(View v) {
			capture();
		}
	};

	private OnClickListener close = new OnClickListener() {

		@Override
		public void onClick(View v) {
			finish();
		}
	};

	private void capture() {
		if (!scanned) {
			scanned = true;

			surface.start(previewCallback, autoFocusCB);
		}

	}

	@Override
	public void onCompleted(List<Aula> lista) {

		if (lista == null || lista.isEmpty()) {

		} else {
			inicializarAtividade(lista);
		}
	}

	@Override
	public void onBackPressed() {
		finish();
	}
	
	private void executeTask(String hash) {
		// obtém a data e hora do dispositivo
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String dataHora = sdf.format(new Date());

		// gera o endereço de onde vão ser obtidos os dados
		dataUrl = Uri.parse(baseUrl).buildUpon()
				.appendQueryParameter("hash", hash)
				.appendQueryParameter("dataHora", dataHora).build().toString();

		ResultQRLoader loader = new ResultQRLoader(this, CaptureActivity.this);
		loader.execute(dataUrl);
	}

	private void execute(final String hash) {
		if (conectado()) {
			executeTask(hash);
		} else {
			AlertDialog.Builder alerta = new AlertDialog.Builder(
					CaptureActivity.this);
			alerta.setTitle("Erro de conexão:");
			alerta.setMessage("Não foi possível conectar-se à Internet:");

			alerta.setPositiveButton("Tentar Novamente",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							if (conectado()) {
								executeTask(hash);
							}
						}
					});

			alerta.setNegativeButton("Cancelar",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							finish();
						}
					});

			alerta.show();

		}
	}

	public void inicializarAtividade(List<Aula> lista) {
		session.setAulas(lista);

		Intent it = new Intent(CaptureActivity.this, ResultsActivity.class);
		it.putExtra("url", dataUrl);

		startActivityForResult(it, 1);
	}

	/* Verifica se o dispositivo está conectado a internet. */
	public boolean conectado() {

		boolean retorno = false;

		try {
			if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
					.isConnected()) {

				retorno = true;
			} else if (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
					.isConnected()) {

				retorno = true;
			}
		} catch (Exception e) {
			Log.e(getClass().getName(), e.getMessage());
		}

		return retorno;

	}


	PreviewCallback previewCallback = new PreviewCallback() {
		public void onPreviewFrame(byte[] data, Camera camera) {
			Camera.Parameters parameters = camera.getParameters();
			Size size = parameters.getPreviewSize();

			ImageScanner scanner = new ImageScanner();
			scanner.setConfig(0, Config.X_DENSITY, 3);
			scanner.setConfig(0, Config.Y_DENSITY, 3);

			Image barcode = new Image(size.width, size.height, "Y800");
			barcode.setData(data);

			int result = scanner.scanImage(barcode);

			if (result != 0) {
				surface.stop();

				SymbolSet syms = scanner.getResults();

				Iterator<Symbol> it = syms.iterator();

				if (it.hasNext()) {
					Symbol sym = it.next();
					scanned = true;
					execute(sym.getData());
				}
			}
		}
	};

	// Ao voltar da tela de Resultado, fecha a tela de captura e volta para a
	protected void onActivityResult(int requestCode, int resultCode, Intent it) {
		finish();
	};

	private AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
		public void onAutoFocus(boolean success, Camera camera) {
			autoFocusHandler.postDelayed(doAutoFocus, 1000);
		}
	};

	private Runnable doAutoFocus = new Runnable() {
		public void run() {
			if (surface.isPreview()) {
				surface.setFocus(autoFocusCB);
			}
		}
	};

	// uma forma segura de obter uma instância objeto da Camera. see: ZBAR
	private static Camera getCameraInstance() {
		Camera c = null;

		try {
			c = Camera.open();
		} catch (Exception e) {
			e.printStackTrace();
			Log.d(CaptureActivity.class.getName(),
					"Problemas ao abrir a câmera: " + e.getMessage());
		}

		return c;
	}

}