<?php

Class QrModel {

    var $db = null;

    function __construct() {
        include_once(dirname(__FILE__) . '/../config.inc.php');

        $this->db = new mysqli($connection['hostname'], $connection['username'], $connection['password'], $connection['database']) or die(mysqli_error($this->db));
        $this->db->set_charset('utf8');
    }

    function __destruct() {
        mysqli_close($this->db);
    }

    function create() {
        
    }

    function retrieve() {
        
    }

    function update() {
        
    }

    function delete() {
        
    }

}
