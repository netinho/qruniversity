<?php

include_once('Controller/QrController.php');

$hash = filter_input(INPUT_GET, 'hash', FILTER_DEFAULT);
$dataHora = filter_input(INPUT_GET, 'dataHora', FILTER_DEFAULT);

$qrController = new qrController();
$salas = $qrController->listarSalas($hash);

if ($dataHora) {
    $data = explode(' ', $dataHora);
    $data = $data[0];
    $dataHora = strtotime($dataHora);

    $days = array(
        'Domingo',
        'Segunda',
        'Terça',
        'Quarta',
        'Quinta',
        'Sexta',
        'Sábado',
    );

    $currentDay = $days[date('w', $dataHora)];

    $tmpSalas = array();
    foreach ($salas as $sala) {
        if ($sala['diaSemana'] === $currentDay) {
            $tmpSalas[] = $sala;
        }
    }
    $salas = $tmpSalas;
    unset($tmpSalas, $tmpDataHora);
}

$qrController->gerarJson($salas);
