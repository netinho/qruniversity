<?php

require_once(dirname(dirname(__FILE__)) . '/Model/Aula.php');
require_once(dirname(dirname(__FILE__)) . '/Libraries/phpqrcode.php');

class QrController {

    function __construct() {
        //parent::__construct();
    }

    function __destruct() {
        //parent::__destruct();
    }

    function listarSalas($hash = null) {
        $aula = new Aula();
        $aula->setHash($hash);
        return $aula->retrieve();
    }

    function gerarJson($array) {
        header('Content-type: application/json');
        $json = array(
            'total' => count($array),
            'registros' => $array
        );
        echo json_encode($json);
    }

    function gerarQrCode($jsonObject) {
        QrCode::png($jsonObject, false, 5, 16);
    }

}
