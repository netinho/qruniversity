<?php
require_once('Controller/QrController.php');
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Lista das Salas</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0; text/html; charset=utf-8" http-equiv="Content-Type">
    </head>
    <body>
        <h1>Salas existentes</h1>
        <table border="1" width="100%">
            <tbody>
                <tr>
                    <th>Semestre</th>
                    <th>Sala</th>
                    <th>Turma</th>
                    <th>Disciplina</th>
                    <th>Professor</th>
                    <th>Dia da Semana</th>
                    <th></th>
                    <th></th>
                </tr>
                <?php
                $qrController = new QrController();
                $salas = $qrController->listarSalas();
                foreach ($salas as $sala) {
                    ?>
                    <tr>
                        <td><?php echo $sala['semestre']; ?></td>
                        <td><?php echo $sala['sala']; ?></td>
                        <td><?php echo $sala['turma']; ?></td>
                        <td><?php echo $sala['disciplina']; ?></td>
                        <td><?php echo $sala['professor']; ?></td>
                        <td><?php echo $sala['diaSemana']; ?></td>
                        <td><a href="gerarQrCode.php?hash=<?php echo $sala['hash'] ?>">Gerar QrCode</a></td>
                        <td><a href="gerarJson.php?hash=<?php echo $sala['hash'] ?>">Gerar Json</a></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </body>
</html>